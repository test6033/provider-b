import json
import pika


class PikaConnection:
    def __init__(self, *args, **kwargs):
        self.params = pika.URLParameters(
            "amqps://sdtevmrw:CER0nluE-IdH8vR_O0StXNjkOamBVONI@chimpanzee.rmq.cloudamqp.com/sdtevmrw"
        )

        self.connection = pika.BlockingConnection(self.params)
        self.channel = self.connection.channel()

    def publish(self, method, body):
        properties = pika.BasicProperties(method)

        if not self.connection or self.connection.is_closed:
            self.connection = pika.BlockingConnection(self.params)
            self.channel = self.connection.channel()
            self.channel.basic_publish(
                exchange="",
                routing_key="airflow",
                body=json.dumps(body),
                properties=properties,
            )

        self.channel.basic_publish(
            exchange="",
            routing_key="airflow",
            body=json.dumps(body),
            properties=properties,
        )
