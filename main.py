import json
from time import sleep
from flask import Flask, jsonify, request
from flask_cors import CORS
from producer import PikaConnection

app = Flask(__name__)
CORS(app)


@app.route("/api/search", methods=["POST"])
def search():
    pika_conn = PikaConnection()

    data = {}
    try:
        data["search_id"] = request.json["search_id"]
        data["status"] = "Completed"
    except Exception as e:
        return jsonify({"error_message": str(e)})

    with open("files/response_b_full.json", "r") as read_file:
        data["data"] = json.load(read_file)

    sleep(60)
    pika_conn.publish("data", data)
    return jsonify(data)


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0")
